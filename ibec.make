; Core version
; ------------
; Each makefile should begin by declaring the core version of Drupal that all
; projects should be compatible with.
core = 7.x

; API version
; ------------
; Every makefile needs to declare it's Drush Make API version. This version of
; drush make uses API version `2`.
api = 2

; Core project
; Get the latest version of Drupal
projects[] = drupal

; Projects
; --------
; Each project that you would like to include in the makefile should be
; declared under the `projects` key. Module fall in this category.
; Modules
projects[] = admin_menu
projects[] = autocomplete_deluxe
projects[] = block_access
projects[] = breakpoints
projects[] = ctools
projects[] = ckeditor
projects[] = ckeditor_image
projects[] = date
projects[] = devel
projects[] = email
projects[] = entity
projects[] = field_collection
projects[] = field_group
projects[] = field_tools
projects[] = google_analytics
projects[] = globalredirect
projects[] = honeypot
projects[] = iek
projects[] = image_url_formatter
projects[] = imce
projects[] = jquery_update
projects[] = libraries
projects[] = link
projects[] = linkit
projects[] = menu_attributes
projects[] = menu_block
projects[] = metatag
projects[] = module_filter
projects[] = pathauto
projects[] = picture
projects[] = redirect
projects[] = scheduler
projects[] = site_map
projects[] = taxonomy_menu
projects[] = token
projects[] = token_filter
projects[] = views
projects[] = views_conditional
projects[] = views_field_view
projects[] = views_php
projects[] = webform
projects[] = wysiwyg
projects[] = imageapi_optimize
projects[] = typogrify

;ibecbootstrap - our custom theme
projects[neutron][download][type] = "get"
projects[neutron][download][url] = "https://bitbucket.org/akselkreis/neutron-drupal-theme/get/6889cfe729b7.zip"
projects[neutron][type] = "theme"

; Libraries
; ckeditor
libraries[ckeditor][download][type] = "get"
libraries[ckeditor][download][url] = "https://github.com/ckeditor/ckeditor-releases/archive/4.4.x.zip"
libraries[ckeditor][directory_name] = "ckeditor"
libraries[ckeditor][type] = "library"

; Libraries
; tinymce
libraries[tinymce][download][type] = "get"
libraries[tinymce][download][url] = "http://download.moxiecode.com/tinymce/tinymce_3.5.11.zip"
libraries[tinymce][directory_name] = "tinymce"
libraries[tinymce][type] = "library"

; Profile - our custom profile for admin
projects[ibeccreative][type] = "profile"
projects[ibeccreative][download][type] = "get"
projects[ibeccreative][download][url] = "https://bitbucket.org/akselkreis/ibec-drupal-installation-profile/get/65c5c22cd8bf.zip"
projects[ibeccreative][download][revision] = "master"
